const express = require('express');
const hbs = require('hbs');
const fs = require('fs');

const port = process.env.PORT || 3000;
var app = express();



// Uses HBS for the views
app.set('view engine', 'hbs');


// Gives the server access to the public directory
app.use(express.static(__dirname + "/public"));

app.use(function (req, res, next){
    var now = new Date(). toString();
    var log = `${now}: ${req.method} ${req.url}`;
    fs.appendFile('server.log', log, function(err){
        if(err){
            console.log('Unable to append to server.log')
        }
    })
   next();
})

// Maintenance page used. We left the NEXT function out so that the page gets stuck there. 

// app.use(function (req, res, next){
//     res.render('maintenance.hbs')
// })
    

// Registering Partials: Partials are used for portions of the webpage that will be repeated throughout the pages (e.g. Footers, headers etc.)

hbs.registerPartials(__dirname+ "/views/partials");

//Registering Helpers: Helpers declare functions that can be repeatedly used throughout the app. This helps with the DRY principle. the registerHelper takes 2 arguments. (1) The name of the function that will be used to call the function (2) The function that will execute

hbs.registerHelper('getCurrentYear', function () {
    return new Date().getFullYear();
})

hbs.registerHelper('screamIt', function (text){
    return text.toUpperCase();
})


// HTTP Routes
app.get('/', function(req, res){
    res.render('home.hbs', {
        pageTitle: 'Home Page',
        welcomeMessage: "Welcome to my website!"
    })
})

app.get('/about', function(req, res){
    res.render('about.hbs', {
        pageTitle: 'About Page',
     
    })
})

app.get('/bad', function(req, res){
    res.send({
        errorMessage: "Unable to handle request"
    })
})

app.get('/projects', function(req, res){
    res.render('projects.hbs', {
        pageTitle: 'Projects Page'
    })
});

app.listen(port, function() {
    console.log("Server is up on port " + port);
});